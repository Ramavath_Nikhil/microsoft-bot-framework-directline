// This loads the environment variables from the .env file
require('dotenv-extended').load();

var builder = require('botbuilder');
var restify = require('restify');

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
    console.log('%s listening to %s', server.name, server.url);
});

// Create connector and listen for messages
var connector = new builder.ChatConnector({
 appId: process.env.MICROSOFT_APP_ID ? process.env.MICROSOFT_APP_ID : '',
 appPassword: process.env.MICROSOFT_APP_PASSWORD ? process.env.MICROSOFT_APP_PASSWORD : ''
});
server.post('/api/messages', connector.listen());

var instructions = 'Welcome to LLYOD chat bot service';


var DialogLabels = {
    Hotels: 'Want to buy an AC',
    Flights: 'Want to buy TV set',
    Support: 'Support'
};

var AcPriceRange = {
    FiveToTen: '5000 to 10,000',
    TenToFifteen: '10,000 to 15,0000',
    FifteenToTwentyFive: '15,000 to 25,000'
};

var ShowCartOrContinueShopping = {
    ShowCart: 'Show Cart',
    ContinueShopping: 'Continue Shopping'
};

var FinishOrderOrContinueShopping = {
    FinishOrder: 'FinishOrder',
    ContinueShopping: 'Continue Shopping'
};

var EndConversationOrContinueShopping = {
    EndConversation: 'End Conversation',
    ContinueShopping: 'Continue Shopping'
};

var HeroCardName = 'Split Air Conditioner';
var ThumbnailCardName = 'Window Air Conditioner';
var ReceiptCardName = 'Tower Air Conditioner';
var SigninCardName = 'Portable Air Conditioner';
var AnimationCardName = "Cassette Air Conditioner";
 
var CardNames = [HeroCardName, ThumbnailCardName, ReceiptCardName, SigninCardName, AnimationCardName];



var bot = new builder.UniversalBot(connector, [
    function (session) {
      session.beginDialog('/startconversation');

 }
    ]);



bot.dialog('/startconversation', [
    function (session) {
     session.sendTyping();
     builder.Prompts.choice(
        session,
        'How would you like to explore the bot?',
        [DialogLabels.Flights, DialogLabels.Hotels],
        {
            maxRetries: 3,
            retryPrompt: 'Not a valid option'
        });

 },
 function (session, result) {

     session.sendTyping();
     if (!result.response) {
            // exhausted attemps and no selection, start over
            session.send('Ooops! Too many attemps :( But don\'t worry, I\'m handling that exception and you can try again!');
            return session.endDialog();
        }

        // on error, start over
        session.on('error', function (err) {
            // session.send('Failed with message: %s', err.message);
            session.send('Oops! something went wrong, need to start again');
            session.endDialog();
        });

        // continue on proper dialog
        var selection = result.response.entity;
        switch (selection) {
            case "Want to buy an AC":
            return session.beginDialog('buyingac');
            case "Want to buy TV set":
            return session.beginDialog('buyingtv');
        }
    }
 ]);



bot.on('conversationUpdate', function (activity) {
    // when user joins conversation, send instructions
    if (activity.membersAdded) {
        activity.membersAdded.forEach(function (identity) {
            if (identity.id === activity.address.bot.id) {
                var reply = new builder.Message()
                .address(activity.address)
                .text(instructions);
                bot.send(reply);
            }
        });
    }
});

bot.dialog('buyingtv', [
    function (session) {
     session.endDialog("Tv buying option coming soon");
 }
 ]);

bot.dialog('ShowCart', [
    function (session) {
     session.endDialog("your cart is");


     if (!session.userData.cart) {
       session.send('Your cart is empty');
       builder.Prompts.choice(
        session,
        'Would you like to continue shopping or you want to end the conversation?',
        EndConversationOrContinueShopping,
        {
            maxRetries: 3,
            retryPrompt: 'Not a valid option'
        });

   }
   else
   {

    var userProfile = session.userData.cart;

    var cart_contains = "";
    cart_contains = cart_contains +"Your cart contains "+userProfile.length+" items\n"; 



    for(var i = 0; i < userProfile.length; i++){

        cart_contains = cart_contains +userProfile[i]+"\n";

    }

    session.send(cart_contains);
    session.beginDialog('FinishOrder');
    
}




}
]).triggerAction({
    matches: /(show|view|my)\s.*cart/i
});


bot.dialog('FinishOrder', [
    function (session) {

        builder.Prompts.choice(
        session,
        'Would you like to continue shopping or you want to end the conversation?',
        FinishOrderOrContinueShopping,
        {
            maxRetries: 3,
            retryPrompt: 'Not a valid option'
        });

      
 },
 function (session, result) {

     session.sendTyping();
     if (!result.response) {
            // exhausted attemps and no selection, start over
            session.send('Ooops! Too many attemps :( But don\'t worry, I\'m handling that exception and you can try again!');
            return session.endDialog();
        }

        // on error, start over
        session.on('error', function (err) {
            // session.send('Failed with message: %s', err.message);
            session.send('Oops! something went wrong, need to start again');
            session.endDialog();
        });

        // continue on proper dialog
        var selection = result.response.entity;
        switch (selection) {
            case 'FinishOrder':
            session.endDialog('Thanks for placing the order');
            session.userData.cart = null;
            return session.beginDialog('/startconversation');
            case "ContinueShopping":
            session.endDialog('');
            return session.beginDialog('/startconversation');
        }
    }
  
    ]);

bot.dialog('buyingac', [
    function (session) {
     session.sendTyping();
     builder.Prompts.choice(session, 'Which AC you want to buy?', CardNames, {
        maxRetries: 3,
        retryPrompt: 'Ooops, what you wrote is not a valid option, please try again'
    });
 },


 function (session, results) {
     session.sendTyping();
        // create the card based on selection
        var selectedCardName = results.response.entity;
        // var card = createCard(selectedCardName, session);

        builder.Prompts.choice(session, 'Please select the price range you are looking for', AcPriceRange, {
            maxRetries: 3,
            retryPrompt: 'Ooops, what you wrote is not a valid option, please try again'
        }); 


    },function (session, results) {

        // create the card based on selection
        var selectedCardName = results.response.entity;
        var card = createCard(selectedCardName, session);

        // attach the card to the reply message
        // var msg = new builder.Message(session).addAttachment(card);

        var reply = new builder.Message(session)
        .attachmentLayout(builder.AttachmentLayout.carousel)
        .attachments(card);

        session.send(reply);
    }   
    ]);


bot.dialog('buyButtonClick', [
    function (session, args, next) {
        // Get color and optional size from users utterance
        var utterance = args.intent.matched[0];
        var color = /(white|gray)/i.exec(utterance);
        var size = /\b(Extra Large|Large|Medium|Small)\b/i.exec(utterance);
        if (color) {
            // Initialize cart item
            var item = session.dialogData.item = { 
                product: "classic " + color[0].toLowerCase() + " t-shirt",
                size: size ? size[0].toLowerCase() : null,
                price: 25.0,
                qty: 1
            };
            if (!item.size) {
                // Prompt for size
                builder.Prompts.choice(session, "What size would you like?", "Small|Medium|Large|Extra Large");
            } else {
                //Skip to next waterfall step
                next();
            }
        } else {
            // Invalid product
            session.send("I'm sorry... That product wasn't found.").endDialog();
        }   
    },
    function (session, results) {
        // Save size if prompted
        var item = session.dialogData.item;
        if (results.response) {
            item.size = results.response.entity.toLowerCase();
        }

        // Add to cart
        if (!session.userData.cart) {
            session.userData.cart = [];
        }
        session.userData.cart.push(item);

        // Send confirmation to users
        session.send("A '%(size)s %(product)s' has been added to your cart.", item).endDialog();
    }
    ]).triggerAction({ matches: /(buy|add)\s.*shirt/i });


bot.dialog('/addToCart',[ function (session, args, next) {

 // var utterance = args.intent.matched[0];

 // Add to cart
 if (!session.userData.cart) {
    session.userData.cart = [];
}
session.userData.cart.push(args.data);
session.send(args.data+" added to cart");


        // var card = createCard(selectedCardName, session);

        builder.Prompts.choice(session, 'Wanna see your cart or continue shopping', ShowCartOrContinueShopping, {
            maxRetries: 3,
            retryPrompt: 'Ooops, what you wrote is not a valid option, please try again'
        }); 


    },
    function (session, result) {
        if (!result.response) {
            // exhausted attemps and no selection, start over
            session.send('Ooops! Too many attemps :( But don\'t worry, I\'m handling that exception and you can try again!');
            return session.endDialog();
        }

        // on error, start over
        session.on('error', function (err) {
            // session.send('Failed with message: %s', err.message);
            session.send('Oops! something went wrong, need to start again');
            session.endDialog();
        });

        // continue on proper dialog
        var selection = result.response.entity;
        switch (selection) {
            case "ShowCart":
            return session.beginDialog('ShowCart');
            case "ContinueShopping":
                session.endDialog('');
            return session.beginDialog('/startconversation');
        }
    }])
.triggerAction({
    matches: /(buy|add)\s.*cart/i
});

bot.beginDialogAction('addToCart', '/addToCart');



function createCard(selectedCardName, session) {
    switch (selectedCardName) {
        case HeroCardName:
        return getCardsAttachments(session,selectedCardName);
        case ThumbnailCardName:
        return getCardsAttachments(session,selectedCardName);
        case ReceiptCardName:
        return createHeroCard(session,selectedCardName);
        case SigninCardName:
        return createHeroCard(session,selectedCardName);
        case AnimationCardName:
        return createHeroCard(session,selectedCardName);
         default:
        return getCardsAttachments(session,selectedCardName);
    }
}

function createHeroCard(session,selectedCardName) {
    return new builder.HeroCard(session)
    .title('Voltas 1.5 Ton 5 Star Split AC ')
    .subtitle('White  (185JY, Aluminium Condenser)')
    .text('It may be swelteringly hot outside, but your home doesn’t have to be. With the Voltas split AC, you can enjoy the perks of looking out into a world that’s sunny and happy, without being bogged down by the humidity. Operating on 1430W power, this AC is ideal for medium-sized rooms.')
    .images([
        builder.CardImage.create(session, 'https://rukminim1.flixcart.com/image/832/832/j2jbl3k0/air-conditioner-new/8/g/c/185jy-1-5-split-voltas-original-imaetvhshgugawnx.jpeg')
        ])
    .buttons([
        builder.CardAction.openUrl(session, 'https://www.flipkart.com/voltas-1-5-ton-5-star-split-ac-white/p/itme6fmpv2cwzgmj?pid=ACNE6FMMFZ45B8GC&srno=b_1_1&otracker=browse&lid=LSTACNE6FMMFZ45B8GC5ITFTA&iid=6566a5c6-2631-433c-9d6e-2d6675ec3e4a.ACNE6FMMFZ45B8GC.SEARCH', 'Know More')
        ]);
}

function createThumbnailCard(session) {
    return new builder.ThumbnailCard(session)
    .title('BotFramework Thumbnail Card')
    .subtitle('Your bots — wherever your users are talking')
    .text('Build and connect intelligent bots to interact with your users naturally wherever they are, from text/sms to Skype, Slack, Office 365 mail and other popular services.')
    .images([
        builder.CardImage.create(session, 'https://sec.ch9.ms/ch9/7ff5/e07cfef0-aa3b-40bb-9baa-7c9ef8ff7ff5/buildreactionbotframework_960.jpg')
        ])
    .buttons([
        builder.CardAction.openUrl(session, 'https://docs.microsoft.com/bot-framework/', 'Get Started')
        ]);
}

var order = 1234;
function createReceiptCard(session) {
    return new builder.ReceiptCard(session)
    .title('John Doe')
    .facts([
        builder.Fact.create(session, order++, 'Order Number'),
        builder.Fact.create(session, 'VISA 5555-****', 'Payment Method')
        ])
    .items([
        builder.ReceiptItem.create(session, '$ 38.45', 'Data Transfer')
        .quantity(368)
        .image(builder.CardImage.create(session, 'https://github.com/amido/azure-vector-icons/raw/master/renders/traffic-manager.png')),
        builder.ReceiptItem.create(session, '$ 45.00', 'App Service')
        .quantity(720)
        .image(builder.CardImage.create(session, 'https://github.com/amido/azure-vector-icons/raw/master/renders/cloud-service.png'))
        ])
    .tax('$ 7.50')
    .total('$ 90.95')
    .buttons([
        builder.CardAction.openUrl(session, 'https://azure.microsoft.com/en-us/pricing/', 'More Information')
        .image('https://raw.githubusercontent.com/amido/azure-vector-icons/master/renders/microsoft-azure.png')
        ]);
}

function createSigninCard(session) {
    return new builder.SigninCard(session)
    .text('BotFramework Sign-in Card')
    .button('Sign-in', 'https://login.microsoftonline.com');
}

function createAnimationCard(session) {
    return new builder.AnimationCard(session)
    .title('Microsoft Bot Framework')
    .subtitle('Animation Card')
    .image(builder.CardImage.create(session, 'https://docs.microsoft.com/en-us/bot-framework/media/how-it-works/architecture-resize.png'))
    .media([
        { url: 'http://i.giphy.com/Ki55RUbOV5njy.gif' }
        ]);
}

function createVideoCard(session) {
    return new builder.VideoCard(session)
    .title('Big Buck Bunny')
    .subtitle('by the Blender Institute')
    .text('Big Buck Bunny (code-named Peach) is a short computer-animated comedy film by the Blender Institute, part of the Blender Foundation. Like the foundation\'s previous film Elephants Dream, the film was made using Blender, a free software application for animation made by the same foundation. It was released as an open-source film under Creative Commons License Attribution 3.0.')
    .image(builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Big_buck_bunny_poster_big.jpg/220px-Big_buck_bunny_poster_big.jpg'))
    .media([
        { url: 'http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4' }
        ])
    .buttons([
        builder.CardAction.openUrl(session, 'https://peach.blender.org/', 'Learn More')
        ]);
}

function createAudioCard(session) {
    return new builder.AudioCard(session)
    .title('I am your father')
    .subtitle('Star Wars: Episode V - The Empire Strikes Back')
    .text('The Empire Strikes Back (also known as Star Wars: Episode V – The Empire Strikes Back) is a 1980 American epic space opera film directed by Irvin Kershner. Leigh Brackett and Lawrence Kasdan wrote the screenplay, with George Lucas writing the film\'s story and serving as executive producer. The second installment in the original Star Wars trilogy, it was produced by Gary Kurtz for Lucasfilm Ltd. and stars Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams, Anthony Daniels, David Prowse, Kenny Baker, Peter Mayhew and Frank Oz.')
    .image(builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/en/3/3c/SW_-_Empire_Strikes_Back.jpg'))
    .media([
        { url: 'http://www.wavlist.com/movies/004/father.wav' }
        ])
    .buttons([
        builder.CardAction.openUrl(session, 'https://en.wikipedia.org/wiki/The_Empire_Strikes_Back', 'Read More')
        ]);
}

function getCardsAttachments(session,selectedCardName) {
    return [
    new builder.HeroCard(session)
    .title('Voltas 1.5 Ton 5 Star Split AC ')
    .subtitle('White  (185JY, Aluminium Condenser)')
    .text('It may be swelteringly hot outside, but your home doesn’t have to be. With the Voltas split AC, you can enjoy the perks of looking out into a world that’s sunny and happy, without being bogged down by the humidity. Operating on 1430W power, this AC is ideal for medium-sized rooms.')
    .images([
        builder.CardImage.create(session, 'https://rukminim1.flixcart.com/image/832/832/j2jbl3k0/air-conditioner-new/8/g/c/185jy-1-5-split-voltas-original-imaetvhshgugawnx.jpeg')
        ])
    .buttons([
        builder.CardAction.openUrl(session, 'https://www.flipkart.com/voltas-1-5-ton-5-star-split-ac-white/p/itme6fmpv2cwzgmj?pid=ACNE6FMMFZ45B8GC&srno=b_1_1&otracker=browse&lid=LSTACNE6FMMFZ45B8GC5ITFTA&iid=6566a5c6-2631-433c-9d6e-2d6675ec3e4a.ACNE6FMMFZ45B8GC.SEARCH', 'Know More'),
        builder.CardAction.dialogAction(session, 'addToCart', 'Voltas 1.5 Ton 5 Star Split AC', 'Add To Cart')
        ])
    ,

    new builder.HeroCard(session)
    .title('Voltas 1.5 Ton 5 Star Split AC ')
    .subtitle('White  (185JY, Aluminium Condenser)')
    .text('It may be swelteringly hot outside, but your home doesn’t have to be. With the Voltas split AC, you can enjoy the perks of looking out into a world that’s sunny and happy, without being bogged down by the humidity. Operating on 1430W power, this AC is ideal for medium-sized rooms.')
    .images([
        builder.CardImage.create(session, 'https://rukminim1.flixcart.com/image/832/832/j2jbl3k0/air-conditioner-new/8/g/c/185jy-1-5-split-voltas-original-imaetvhshgugawnx.jpeg')
        ])
    .buttons([
     builder.CardAction.openUrl(session, 'https://www.flipkart.com/voltas-1-5-ton-5-star-split-ac-white/p/itme6fmpv2cwzgmj?pid=ACNE6FMMFZ45B8GC&srno=b_1_1&otracker=browse&lid=LSTACNE6FMMFZ45B8GC5ITFTA&iid=6566a5c6-2631-433c-9d6e-2d6675ec3e4a.ACNE6FMMFZ45B8GC.SEARCH', 'Know More'),
     builder.CardAction.imBack(session, 'add Voltas 1.5 Ton 5 Star Split AC to my card', 'Add to cart')    ]),

    new builder.HeroCard(session)
    .title('Voltas 1.5 Ton 5 Star Split AC ')
    .subtitle('White  (185JY, Aluminium Condenser)')
    .text('It may be swelteringly hot outside, but your home doesn’t have to be. With the Voltas split AC, you can enjoy the perks of looking out into a world that’s sunny and happy, without being bogged down by the humidity. Operating on 1430W power, this AC is ideal for medium-sized rooms.')
    .images([
        builder.CardImage.create(session, 'https://rukminim1.flixcart.com/image/832/832/j2jbl3k0/air-conditioner-new/8/g/c/185jy-1-5-split-voltas-original-imaetvhshgugawnx.jpeg')
        ])
    .buttons([
        builder.CardAction.openUrl(session, 'https://www.flipkart.com/voltas-1-5-ton-5-star-split-ac-white/p/itme6fmpv2cwzgmj?pid=ACNE6FMMFZ45B8GC&srno=b_1_1&otracker=browse&lid=LSTACNE6FMMFZ45B8GC5ITFTA&iid=6566a5c6-2631-433c-9d6e-2d6675ec3e4a.ACNE6FMMFZ45B8GC.SEARCH', 'Know More'),
        builder.CardAction.imBack(session, 'add Voltas 1.5 Ton 5 Star Split AC to my card', 'Add to cart')       ]),

    new builder.HeroCard(session)
    .title('Voltas 1.5 Ton 5 Star Split AC ')
    .subtitle('White  (185JY, Aluminium Condenser)')
    .text('It may be swelteringly hot outside, but your home doesn’t have to be. With the Voltas split AC, you can enjoy the perks of looking out into a world that’s sunny and happy, without being bogged down by the humidity. Operating on 1430W power, this AC is ideal for medium-sized rooms.')
    .images([
     builder.CardAction.openUrl(session, 'https://www.flipkart.com/voltas-1-5-ton-5-star-split-ac-white/p/itme6fmpv2cwzgmj?pid=ACNE6FMMFZ45B8GC&srno=b_1_1&otracker=browse&lid=LSTACNE6FMMFZ45B8GC5ITFTA&iid=6566a5c6-2631-433c-9d6e-2d6675ec3e4a.ACNE6FMMFZ45B8GC.SEARCH', 'Know More'),
     builder.CardAction.imBack(session, 'add Voltas 1.5 Ton 5 Star Split AC to my cart', 'Add to cart')        ])
    .buttons([
        builder.CardAction.openUrl(session, 'https://www.flipkart.com/voltas-1-5-ton-5-star-split-ac-white/p/itme6fmpv2cwzgmj?pid=ACNE6FMMFZ45B8GC&srno=b_1_1&otracker=browse&lid=LSTACNE6FMMFZ45B8GC5ITFTA&iid=6566a5c6-2631-433c-9d6e-2d6675ec3e4a.ACNE6FMMFZ45B8GC.SEARCH', 'Know More'),
        builder.CardAction.imBack(session, 'add Voltas 1.5 Ton 5 Star Split AC to my cart', 'Add to cart')    ])
    ];
}
