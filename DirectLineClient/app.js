var Swagger = require('swagger-client');
var open = require('open');
var rp = require('request-promise');
var restify = require('restify');
var client = require('./');

// Setup Restify Server
var server = restify.createServer();
server.use(restify.plugins.bodyParser());
server.listen(process.env.port || process.env.PORT || 3979, function () {
  console.log('%s listening to %s', server.name, server.url);
});


server.get('/home', function(req, res, next) {
  res.send('home')
  return next();
});


server.post('/send',
  function(req, res, next) {
    req.someData = 'foo';


 // console.log(JSON.parse(req.body).conversation_id);
 // console.log(JSON.parse(req.body).token);
 // console.log(JSON.parse(req.body).message);
 // console.log(JSON.parse(req.body).message.type);
 // console.log(JSON.parse(req.body).message.from.id);


// var conversationId = 'GPzzIvajmbi6oOkvNOAXN5';
// var token = 'Dp1GLjr5Tkk.dAA.RwBQAHoAegBJAHYAYQBqAG0AYgBpADYAbwBPAGsAdgBOAE8AQQBYAE4ANQA.47fN_aBt0wE.wzWTE2XrVv0.29hXDRmLPXLjrLMBjRKLOufxwx4ybX6tKz0nMHhzcak';

var request = JSON.parse(req.body);
var token = request.token;
var conversationId = request.conversation_id;
var message = request.message;
var watermark = 0;  
var result = client.postMessage(token, conversationId,message).then(function(result) {
var refreshIntervalId = null;
var responseErrorCount =0;
 var pollInterval = 1000;

 refreshIntervalId= setInterval(function () {


    client.getMessages(token, conversationId, watermark)
    .then(function(result) {

            // var request = JSON.parse(result);
            watermark = result.watermark;     

              // res.send(result.activities[(result.activities.length)-1]);
               // res.send(result);
               return result.activities;

             }).then(function(activities){

              if (activities && activities.length) {
        // ignore own messages


        // activities = activities.filter(function (m) { return m.from.id !== directLineClientName });
        console.log(activities.length);
        if (activities.length==1) {

            // activities.forEach(printMessage);
            console.log(activities.length+" is the length");
            res.send(activities[0]);
            clearInterval(refreshIntervalId);
          }
          else
          {
           // res.send("empty activities");
           console.log("greater or less than one activities");
         }
       }
       else
       {
         // res.send("null activities");
         console.log("null activities");

         if(responseErrorCount<3)
         {
          responseErrorCount = responseErrorCount+1;
         }
         else
         {
           res.send("sorry I missed you");
         }
       }


     })
             .fail(function(err) {
              console.log(err);
              res.send(err);
              clearInterval(refreshIntervalId);
              return next();
            });


           }, pollInterval);









})
.fail(function(err) {
  console.log(err);
  res.send(err);
  return next();

});;




return next();
}
  // function(req, res, next) {
  //   res.send(req.someData);
  //   return next();
  // }
  );




 

// Read from console (stdin) and send input to conversation using DirectLine client
function sendMessagesFromConsole(client, conversationId) {
 process.stdout.write('send message from console ');
 var stdin = process.openStdin();
 process.stdout.write('Command> ');
 stdin.addListener('data', function (e) {
  var input = e.toString().trim();
  if (input) {
            // exit
            if (input.toLowerCase() === 'exit') {
              return process.exit();
            }

            // send message
            client.Conversations.Conversations_PostActivity(
            {
              conversationId: conversationId,
              activity: {
                textFormat: 'plain',
                text: input,
                type: 'message',
                from: {
                  id: directLineClientName,
                  name: directLineClientName
                }
              }
            }).catch(function (err) {
              console.error('Error sending message:', err);
            });

            process.stdout.write('Command> ');
          }
          else
          {
            process.stdout.write('input ');
          }
        });
}

// Poll Messages from conversation using DirectLine client
function pollMessages(client, conversationId) {
  console.log('Starting polling message for conversationId: ' + conversationId);
  var watermark = null;
  setInterval(function () {
    client.Conversations.Conversations_GetActivities({ conversationId: conversationId, watermark: watermark })
    .then(function (response) {
                watermark = response.obj.watermark;                                 // use watermark so subsequent requests skip old messages 

                return response.obj.activities;
              })
    .then(printMessages)
    .catch(function (err) {
      console.error('Error initializing DirectLine client', err);
    });
  }, pollInterval);
}

// Helpers methods
function printMessages(activities) {
  if (activities && activities.length) {
        // ignore own messages


        activities = activities.filter(function (m) { return m.from.id !== directLineClientName });

        if (activities.length) {
          process.stdout.clearLine();
          process.stdout.cursorTo(0);

            // print other messages
            activities.forEach(printMessage);

            process.stdout.write('Command> ');
          }
          else
          {
            process.stdout.write('empty activity');
          }
        }
        else
        {
         // process.stdout.write('no fetching message');
       }
     }

     function printMessage(activity) {
      if (activity.text) {
        console.log(activity.text);
      }

      if (activity.attachments) {
        activity.attachments.forEach(function (attachment) {
          switch (attachment.contentType) {
            case "application/vnd.microsoft.card.hero":
            renderHeroCard(attachment);
            break;

            case "image/png":
            console.log('Opening the requested image ' + attachment.contentUrl);
            open(attachment.contentUrl);
            break;
          }
        });
      }
    }

    function renderHeroCard(attachment) {
      var width = 70;
      var contentLine = function (content) {
        return ' '.repeat((width - content.length) / 2) +
        content +
        ' '.repeat((width - content.length) / 2);
      }

      console.log('/' + '*'.repeat(width + 1));
      console.log('*' + contentLine(attachment.content.title) + '*');
      console.log('*' + ' '.repeat(width) + '*');
      console.log('*' + contentLine(attachment.content.text) + '*');
      console.log('*'.repeat(width + 1) + '/');
    }